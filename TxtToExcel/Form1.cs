﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TxtToExcel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExcelManager eman = new ExcelManager();
            eman.WriteToExcel(eman.ParseTxt('\t'), textBox1.Text, (int)numericUpDown1.Value);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
