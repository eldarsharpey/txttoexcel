﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OfficeOpenXml;
using System.IO;
using System.Windows.Forms;

namespace TxtToExcel
{
    class ExcelManager
    {

        public List<string[]> ParseTxt(char separator)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text|*.txt|All|*.*";
            ofd.Title = "CHOOSE TXT FILE TO PARSE";

            string path = "NotSelected";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                path = ofd.FileName;
            }


            List<string[]> data = new List<string[]>();

            try
            {
                foreach (string line in File.ReadLines(path))
                {
                   data.Add(line.Split(separator));
                }
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                return null;
            }

        }



        public void WriteToExcel(List<string[]> data, string startc, int sheetnum)
        {

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Format|*.xlsx|All|*.*";
            ofd.Title = "CHOOSE XMLS FILE TO SAVE";

            string path = "NotSelected";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                path = ofd.FileName;
            }


            FileInfo fileInfo = new FileInfo(path);
            try
            {
                ExcelPackage exp = new ExcelPackage(fileInfo);

                ExcelWorksheet wSheet1 = exp.Workbook.Worksheets[sheetnum];

                ExcelAddress add = new ExcelAddress(startc);


                int j = 0;
            foreach(string[] line in data)
            {
                j++;
                int k = 0;
                foreach (string cdata in line)
                {
                    k++;

                    float f = 0f;
                    int i = 0;
                        
                    ExcelRange rng = wSheet1.Cells[add.Start.Row+j-1, add.Start.Column + k-1, add.Start.Row + j-1, add.Start.Column + k-1];

                    if (Int32.TryParse(cdata, out i))
                        rng.Value = i;
                    else if (float.TryParse(cdata, out f))
                        rng.Value = f;
                    else
                        rng.Value = cdata;

                }
            }

            exp.SaveAs(fileInfo);

                MessageBox.Show("Data saved!", "Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
